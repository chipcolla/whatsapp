from django.urls import path

from src.apps.waa.views.waa import WaaHome

app_name = "waa"

urlpatterns = [
    path(r'', WaaHome.as_view(), name='home')
]
