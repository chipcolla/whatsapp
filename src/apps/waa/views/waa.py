from django.middleware.csrf import get_token
from django.views.generic import TemplateView


class WaaHome(TemplateView):
    template_name = 'base/waa.html'

    def dispatch(self, request, *args, **kwargs):
        token = get_token(request)
        # print(token)
        return super().dispatch(request, *args, **kwargs)
