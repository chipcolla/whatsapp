import json

import datetime

import decimal
from django.http import HttpResponse


def json_handler(o):
    if isinstance(o, datetime.datetime) or isinstance(o, datetime.date):
        return o.isoformat()
    if isinstance(o, decimal.Decimal):
        return str(o)


class AjaxMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponse('Поддерживаются только ajax запросы.')
        return super(AjaxMixin, self).dispatch(request, *args, **kwargs)

    def render_to_response(self, context):
        context = self.convert_context_to_json(context)
        return HttpResponse(context, content_type='application/json')

    def convert_context_to_json(self, context):
        return json.dumps(context, default=json_handler)
