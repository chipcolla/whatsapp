import requests
from django.contrib.auth.backends import ModelBackend, UserModel
from passlib.handlers.pbkdf2 import pbkdf2_sha256
from utils.api_request import api_call


class SettingsBackend(ModelBackend):
    def __init__(self):
        self.custom_pbkdf2 = pbkdf2_sha256.using(rounds=120000)

    def authenticate(self, request, username=None, password=None, **kwargs):
        if username is None:
            username = kwargs.get(UserModel.USERNAME_FIELD)
        try:
            user = UserModel._default_manager.get_by_natural_key(username)
        except UserModel.DoesNotExist:
            pass
        else:
            if self.custom_pbkdf2.verify(password, user.password) and self.user_can_authenticate(user):
                token = api_call('http://localhost:8080/account/token/auth',
                                 {'email': user.email, 'password': password})
                if 'token' in token:
                    request.session['jwt_token'] = token.get('token')
                return user

