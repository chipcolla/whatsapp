from billiard.pool import Pool


class MP:
    POOL = Pool(processes=1)

    @classmethod
    def run(cls, f):
        result = cls.POOL.apply_async(f)
        return result.get()

