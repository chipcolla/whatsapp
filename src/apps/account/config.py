from enum import Enum


class MessageTypeChoice(Enum):
    CHAT = 1
    IMAGE = 2
    PTT = 3
    DOCUMENT = 4
    AUDIO = 5
    CALL_LOG = 6


class InboxOutboxChoice(Enum):
    INBOX = 1
    OUTBOX = 2
