from rest_framework import serializers

from src.apps.account.models import Message


class MessageSerializer(serializers.ModelSerializer):
    message_type = serializers.IntegerField(default=1)
    inbox_outbox = serializers.IntegerField(default=2)

    class Meta:
        model = Message
        fields = ('chat_id', 'message', 'message_type', 'inbox_outbox')

    def to_representation(self, obj):
        ret = super(MessageSerializer, self).to_representation(obj)
        ret.pop('message_type', None)
        ret.pop('inbox_outbox', None)

        return ret