import threading

from billiard.pool import Pool
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from user_agent import generate_user_agent


class WD:
    def __init__(self, profile_name):
        self.options = Options()

        self.options.add_argument("{}".format(generate_user_agent(navigator='chrome')))
        self.options.add_argument("--disable-dev-shm-usage")
        self.options.add_argument("--disable-gpu")
        self.options.add_argument("--disable-extensions")

        self.options.add_argument("--no-sandbox")
        self.options.add_argument("--disable-setuid-sandbox")
        self.options.add_argument("--test-type")

        self.options.add_argument("--user-data-dir=/home/wa/profiles")

        self.options.add_argument("--profile-directory={}".format(profile_name))

        # self.options.add_argument("start-maximized")
        self.options.add_argument("--disable-popup-blocking")
        self.options.add_argument("disable-infobars")

        self.capabilities = self.options.to_capabilities()
        self.driver = None

    def get_qr_code(self):
        self.driver = webdriver.Remote(command_executor='http://104.248.83.6:4444/wd/hub',
                                       desired_capabilities=self.capabilities)

        self.driver.get("https://web.whatsapp.com/")
        self.driver.set_page_load_timeout(30)
        self.driver.implicitly_wait(20)

        qr = ''

        try:
            qr = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, "//img[@alt='Scan me!']")))
            qr = qr.get_attribute('src')
        except TimeoutException:
            pass

        self.driver_quit()

        return qr

    def driver_quit(self):
        if self.driver:
            self.driver.quit()


class WebDriver:
    def __init__(self, profile_name):
        self.options = Options()

        self.options.add_argument("{}".format(generate_user_agent(navigator='chrome')))
        self.options.add_argument("--disable-dev-shm-usage")
        self.options.add_argument("--disable-gpu")
        self.options.add_argument("--disable-extensions")

        self.options.add_argument("--no-sandbox")
        self.options.add_argument("--disable-setuid-sandbox")
        self.options.add_argument("--test-type")

        self.options.add_argument("--user-data-dir=/home/wa/profiles")

        self.options.add_argument("--profile-directory={}".format(profile_name))

        # self.options.add_argument("start-maximized")
        self.options.add_argument("--disable-popup-blocking")
        self.options.add_argument("disable-infobars")

        self.capabilities = self.options.to_capabilities()
        self.driver = {}
        self.POOL = Pool(processes=2)

    def run(self, f):
        result = self.POOL.apply(f)
        return result

    def get_qr_code(self):
        print('START PPP')
        driver = {
            'one': webdriver.Remote(command_executor='http://104.248.83.6:4444/wd/hub',
                                    desired_capabilities=self.capabilities)
        }

        driver['one'].get("https://web.whatsapp.com/")
        driver['one'].set_page_load_timeout(30)
        driver['one'].implicitly_wait(20)

        qr = ''

        try:
            qr = WebDriverWait(driver['one'], 10).until(
                EC.presence_of_element_located((By.XPATH, "//img[@alt='Scan me!']")))
            qr = qr.get_attribute('src')
        except TimeoutException:
            pass

        driver['one'].quit()
        # self.driver_quit()

        print('PPPP: {}'.format(qr))
        return qr

    def driver_quit(self):
        if self.driver:
            self.driver['one'].quit()

