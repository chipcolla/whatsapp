from django.urls import path, re_path

from src.apps.account.views.account import AuthView, AccountView
from src.apps.account.views.ajax import GetQrCode


app_name = "account"

urlpatterns = [
    re_path('^(?P<section>signin|signup|signout)/$', AuthView.as_view(), name='user_auth'),
    path('', AccountView.as_view(), name='user_account'),
    path('ajax/get-qr-code/', GetQrCode.as_view(), name="ajax_get_qr_code")
]
