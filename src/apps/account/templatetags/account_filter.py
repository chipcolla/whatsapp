from django import template

register = template.Library()


@register.filter(name='get_form_name')
def get_form_name(form):
    return form.__class__.__name__
