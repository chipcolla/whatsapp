# Generated by Django 2.1.5 on 2019-06-05 07:23

from django.db import migrations, models
import django.db.models.deletion
import src.apps.account.manager


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0007_message'),
    ]

    operations = [
        migrations.CreateModel(
            name='Server',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('host', models.URLField(max_length=255)),
                ('port', models.IntegerField()),
                ('date_create', models.DateTimeField(auto_now_add=True)),
                ('disabled', models.BooleanField(default=False)),
            ],
        ),
        migrations.AlterModelManagers(
            name='account',
            managers=[
                ('objects', src.apps.account.manager.AccountUserManager()),
            ],
        ),
        migrations.AlterField(
            model_name='message',
            name='inbox_outbox',
            field=models.IntegerField(choices=[(1, 'INBOX'), (2, 'OUTBOX')]),
        ),
        migrations.AlterField(
            model_name='message',
            name='message_type',
            field=models.IntegerField(choices=[(1, 'CHAT'), (2, 'IMAGE'), (3, 'PTT'), (4, 'DOCUMENT'), (5, 'AUDIO'), (6, 'CALL_LOG')]),
        ),
        migrations.AddField(
            model_name='account',
            name='server',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='account.Server'),
        ),
    ]
