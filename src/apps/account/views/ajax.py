from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views import View

from src.apps.account.mixins.ajax import AjaxMixin


from utils.api_request import api_call


@method_decorator(login_required, name='dispatch')
class GetQrCode(AjaxMixin, View):
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        qr = api_call('http://localhost:8080/account/api/get-qr',
                      headers={'Content-Type': 'application/json', 'Authorization': '{}'.format(
                          request.session["jwt_token"])})
        if qr:
            return self.render_to_response({
                'code': 200,
                'message': 'Ответ получен.',
                'qr': qr.get('qr'),
                'class': '{}'.format(self.__class__.__name__)
            })

        return self.render_to_response({
            'code': 500,
            'message': 'Произошла ошибка.',
            'class': '{}'.format(self.__class__.__name__)
        })

