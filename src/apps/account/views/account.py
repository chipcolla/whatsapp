from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import FormView, TemplateView

from src.apps.account.forms.auth import AuthSignInForm, AuthSignUpForm


class AuthView(FormView):
    form_class = None
    template_name = 'base/auth.html'
    success_url = '/'

    def dispatch(self, request, *args, **kwargs):
        if kwargs['section'] == 'signout':
            logout(request)
            return redirect(reverse('waa:home'))
        return super().dispatch(request, *args, **kwargs)

    def get_form_class(self):
        if self.kwargs['section'] == 'signup':
            return AuthSignUpForm
        return AuthSignInForm

    def form_valid(self, form):
        if form.__class__.__name__ == 'AuthSignUpForm':
            form.save()
        else:
            user = authenticate(self.request, username=form.cleaned_data['email'],
                                password=form.cleaned_data['password'])
            if user:
                login(self.request, user)
                return redirect(reverse('account:user_account'))
        return redirect(reverse('waa:home'))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'section': self.kwargs['section'],
            'form_action_url': (reverse('account:user_auth', kwargs={'section': 'signup'})
                                if self.kwargs['section'] == 'signup'
                                else reverse('account:user_auth', kwargs={'section': 'signin'}))
        })
        return context


@method_decorator(login_required, name='dispatch')
class AccountView(TemplateView):
    template_name = 'base/account.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'jwt_token': self.request.session.get('jwt_token', '-')
        })
        return context





