from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from user_agent import generate_user_agent


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class DriverFabric(object):
    __metaclass__ = Singleton
    drivers = {}

    def get_driver(self, profile_name):
        options = Options()
        options.add_argument("{}".format(generate_user_agent(navigator='chrome')))
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--disable-default-apps")
        options.add_argument("--disable-gpu")
        options.add_argument("--disable-extensions")

        options.add_argument("--user-data-dir=/home/goodboy/dev/whatsappapi/profile")
        # options.add_argument("--user-data-dir=C:\www\whatsappapi/profile")
        options.add_argument("--profile-directory={}".format(profile_name))

        service_args = ['--verbose']

        # driver = webdriver.Chrome(executable_path=r'C:\www\whatsappapi/chromedriver.exe', options=options)
        self.drivers[profile_name] = webdriver.Chrome(
            executable_path=r'/home/goodboy/dev/whatsappapi/chromedriver', options=options)
        return self.drivers[profile_name]

    def quit_driver(self, profile_name):
        self.drivers[profile_name].quit()
        del self.drivers[profile_name]


