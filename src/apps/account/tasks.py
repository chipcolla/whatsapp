from src.apps.account.mp_call import MP
from src.apps.account.webdriver import WD
from whatsappapi.celery import app


@app.task()
def get_qr_code(profile_name):
    wd = WD(profile_name)
    qr = MP.run(wd.get_qr_code)
    return qr
