from django.db import models

from src.apps.account.config import MessageTypeChoice, InboxOutboxChoice
from src.apps.account.models.account import Account


class Message(models.Model):
    # Пользователь. Пока не понятно будет привязываться или нет.
    user = models.ForeignKey(Account, on_delete=models.DO_NOTHING, blank=True, null=True)
    # ИД чата или пользователя
    chat_id = models.CharField(max_length=255)
    # Имя отправителя
    sender_name = models.CharField(max_length=255, blank=True)
    # ID отправителя
    sender_id = models.CharField(max_length=255,  blank=True)
    # Сообщение
    message = models.CharField(max_length=500)
    # Тип сообщения
    message_type = models.IntegerField(choices=[(i.value, i.name) for i in MessageTypeChoice])
    # Уникальный ID сообщения
    message_id = models.CharField(max_length=255, blank=True)
    # Входящее или исходящее сообщение
    inbox_outbox = models.IntegerField(choices=[(i.value, i.name) for i in InboxOutboxChoice])
    # Дата добавления
    date_add = models.DateTimeField(auto_now_add=True)
    # Отправлено? Для исходящих сообщений
    send = models.BooleanField(default=False)
    # Сообщение прочтено? Для входящих сообщений
    read = models.BooleanField(default=False)
    # Дата отправки. Для исходящих сообщений
    date_send = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return '{}'.format(self.chat_id)
