from django.db import models


class Server(models.Model):
    name = models.CharField(max_length=255)
    host = models.URLField(max_length=255)
    date_create = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


