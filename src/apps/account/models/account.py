from django.contrib.auth.models import AbstractUser, UserManager, PermissionsMixin
from django.db import models
from passlib.handlers.pbkdf2 import pbkdf2_sha256

from src.apps.account.manager import AccountUserManager
from src.apps.account.models.server import Server


class Account(AbstractUser, PermissionsMixin):
    server = models.ForeignKey(Server, on_delete=models.SET_NULL, blank=True, null=True)
    # server = models.ManyToManyField(Server, through='ServerParams', blank=True, null=True)
    username = models.CharField(max_length=255, unique=True)
    email = models.EmailField(max_length=255, unique=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    port = models.IntegerField(null=True)

    objects = AccountUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.custom_pbkdf2 = pbkdf2_sha256.using(rounds=120000)

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    def set_password(self, raw_password):
        self.password = self.custom_pbkdf2.hash(raw_password)
        self._password = raw_password

    def get_username(self):
        return self.email


class ServerParams(models.Model):
    account = models.ForeignKey(Account, on_delete=models.SET_NULL, blank=True, null=True)
    server = models.ForeignKey(Server, on_delete=models.SET_NULL, blank=True, null=True)
    port = models.IntegerField()
    disabled = models.BooleanField(default=False)
