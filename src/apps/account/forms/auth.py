from django import forms
from django.db import connection
from django.forms.models import ModelForm

from src.apps.account.manager import lock_table
from src.apps.account.models import Account


class AuthSignUpForm(ModelForm):
    """
    Форма регистрации пользователя
    """

    def __init__(self,  *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['password'].widget = forms.PasswordInput()

    class Meta:
        model = Account
        fields = ['email', 'password']
        labels = {
            'email': 'Почта',
            'password': 'Пароль',
        }

    def save(self, commit=True):
        form = super().save(commit=False)

        with lock_table(Account):
            with connection.cursor() as cursor:
                cursor.execute("select t1.server_id, t1.port from public.account_account as t1"
                               " inner join (select server_id, count(server_id) as ct from public.account_account"
                               " group by server_id HAVING count(server_id) < 10"
                               " order by ct asc limit 1) as t2 on t1.server_id=t2.server_id"
                               " order by id desc limit 1")
                row = cursor.fetchone()
            Account.objects.create_user(email=self.cleaned_data['email'], password=self.cleaned_data['password'],
                                        server_id=row[0], port=row[1] + 1)
        return form


class AuthSignInForm(forms.Form):
    """
    Форма авторизации пользователя
    """
    email = forms.CharField(label='Почта', max_length=255)
    password = forms.CharField(label='Пароль', max_length=128, widget=forms.PasswordInput())

    def save(self):
        pass


