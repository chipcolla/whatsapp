from contextlib import contextmanager

from django.contrib.auth.models import UserManager
from django.db import transaction
from django.db.transaction import get_connection


class AccountUserManager(UserManager):

    def create_user(self, username=None, email=None, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        if not username:
            username = email
        return self._create_user(username, email, password, **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        if not username:
            username = email

        return self._create_user(username, email, password, **extra_fields)


@contextmanager
def lock_table(model):
    with transaction.atomic():
        cursor = get_connection().cursor()
        cursor.execute('LOCK TABLE %s' % model._meta.db_table)
        try:
            yield
        finally:
            cursor.close()
