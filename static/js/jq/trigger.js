"use strict";

(function() {
    $(document).ready(function() {
        const buttonGetQr = $("#button-get-qr");
        const QrImage = $("img#qr-image");

        buttonGetQr.click(function(e) {
            addButtonIcon(buttonGetQr, "fas fa-spinner fa-spin");
            $(this).prop("disabled", true);
            ajaxCall({ url: ajaxRouter["getQrCode"], success: function(data){
                removeButtonIcon(buttonGetQr, "fas");
                if (data.code === 200) {
                    QrImage.attr("src", data["qr"]).removeClass("blur");
                    setTimeout(function () {
                        QrImage.addClass("blur");
                        buttonGetQr.prop("disabled", false);
                    }, 15000);
                }
                }, data: {} })
        });

    });

})(jQuery);