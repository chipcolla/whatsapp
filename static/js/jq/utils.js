"use strict";

let host = "http://127.0.0.1:8000/";

let ajaxRouter = {
  getQrCode: `${host}account/ajax/get-qr-code/`
};

let ajaxCall = ({ url, success, secure=false, type="POST", data={}, beforeSend=() => {}, error=(xhr, textStatus) => {} }) => {
    $.ajax({
            url: url,
            secure: secure,
            type: type,
            headers: {"X-CSRFToken": Cookies.get('csrftoken')},
            dataType: "json",
            data: data,
            beforeSend: beforeSend,
            success: success,
            error: error
    });
};

let addButtonIcon = (button, cls) => {
    button.html(`<i class="${cls}"></i> ${button.html()}`)
};

/*
a - Add
r - Remove
*/
let changeButtonIcon = (button, cls, newCls, mode='a') => {
    let iElement = button.find(`i.${cls}`);
    if (mode === "a") iElement.addClass(`${newCls}`);
    else  iElement.removeClass(`${newCls}`);
};

let removeButtonIcon = (button, cls) => {
    button.find(`i.${cls}`).remove();
};