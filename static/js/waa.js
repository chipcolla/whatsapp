//(function() {
    $(document).ready(function () {

        $("button#get-qr-code").click(function () {
            debugger;
            var button = $(this);
            $.ajax({
                url: 'http://127.0.0.1:8000/account/ajax/get/qr-code/',
                //secure: true,
                type: "POST",
                headers: {"X-CSRFToken": Cookies.get('csrftoken')},
                dataType: "json",
                data: {
                    session_url: button.data("session-url") ? button.data("session-url") : "",
                    session_id: button.data("session-id") ? button.data("session-id") : ""
                },
                beforeSend: function () {
                },
                success: function (data) {
                    if (data.code && data.status === "OK") {
                        var img = $('<img />', {
                            id: 'qr-code-img',
                            src: data.dataSrc,
                            alt: 'Scan me!'
                        });

                        button.data("session-url", data.sessionUrl).data("session-id", data.sessionId);
                        img.appendTo($('#qr-code-place'));

                        $("#quit-browser").data("driver-id", data.driverId);
                    }
                },
                error: function (xhr, textStatus) {
                }
            });
        });

        $("#quit-browser").click(function () {
            var btn = $(this);
            var driverId = btn.data("driver-id");
            $.ajax({
                url: 'http://127.0.0.1:8000/account/ajax/driver/quit/',
                //secure: true,
                type: "POST",
                headers: {"X-CSRFToken": Cookies.get('csrftoken')},
                dataType: "json",
                data: {
                    driverId: driverId
                },
                beforeSend: function () {
                },
                success: function (data) {
                    if (data.code && data.status === "OK") {
                        alert("Драйвер закрыт!");
                    }
                },
                error: function (xhr, textStatus) {
                }
            });
        });


        // window.WAA = {};
        //
        // window.WAA.getNewId = function () {
        //     var text = "";
        //     var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        //     for (var i = 0; i < 20; i++)
        //         text += possible.charAt(Math.floor(Math.random() * possible.length));
        //     return text;
        // };
        //
        // if (!window.Store) {
        //     // webpackJsonp([], {"bcihgfbdeb": (x, y, z) => window.Store = z('"bcihgfbdeb"')}, "bcihgfbdeb");
        //     // webpackJsonp([], {"iaeeehaci": (x, y, z) => window.Store.Wap = z('"iaeeehaci"')}, "iaeeehaci");
        //     // webpackJsonp([], {"bcihgfbdeb": (x, y, z) => window.Store = z('"bcihgfbdeb"')}, "bcihgfbdeb");
        //     // window.Store = {}; webpackJsonp([], { "dgfhfgbdeb": (x, y, z) => window.Store.Wap = z('"dgfhfgbdeb"') }, "dgfhfgbdeb");
        //     // webpackJsonp([], {"jfefjijii": (x, y, z) => window.Store.Conn = z('"jfefjijii"')}, "jfefjijii");
        //     // //webpackJsonp([], {"iaeeehaci": (x, y, z) => window.Store.Wap = z('"iaeeehaci"')}, "iaeeehaci");
        //     // webpackJsonp([], { "bcihgfbdeb": (x, y, z) => window.Store.Presence = z('"bcihgfbdeb"').Presence }, "bcihgfbdeb");
        //
        // }
    });
//});