import requests
from django.http import HttpResponseServerError


def api_call(url, json=None, headers=None):
    try:
        r = requests.post(url, json=json, headers=headers)
        return r.json()
    except Exception:
        return None

